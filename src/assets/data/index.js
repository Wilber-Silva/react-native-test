export const sideBarList = [
    {icon: '1', title:'Novidades'},
    {icon: '1', title:'Inspiração'},
    {icon: '1', title:'Duratex'},
    {icon: '1', title:'Troque seus pontos'},
]

export const fakeSpotlightData = [
    {
        title: 'Lorem Ipsum 1',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ex ullamcorper, mattis ipsum facilisis, eleifend purus. Suspendisse malesuada sed ex vel cursus. Proin augue massa, lacinia quis magna non, ultrices vehicula mi. Ut vulputate tellus in egestas fermentum. Maecenas arcu ante, imperdiet sed ullamcorper at, rhoncus quis neque. Phasellus leo odio, malesuada sed mi sit amet, blandit ullamcorper tellus. Integer quis tortor tincidunt, mattis mauris nec, molestie sapien. Pellentesque eu maximus mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris gravida accumsan augue a sollicitudin. Cras non feugiat diam.',
        image: require('./../../assets/background/banner-bg.jpg'),
    },
    {
        title: 'Lorem Ipsum 2',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ex ullamcorper, mattis ipsum facilisis, eleifend purus. Suspendisse malesuada sed ex vel cursus. Proin augue massa, lacinia quis magna non, ultrices vehicula mi. Ut vulputate tellus in egestas fermentum. Maecenas arcu ante, imperdiet sed ullamcorper at, rhoncus quis neque. Phasellus leo odio, malesuada sed mi sit amet, blandit ullamcorper tellus. Integer quis tortor tincidunt, mattis mauris nec, molestie sapien. Pellentesque eu maximus mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris gravida accumsan augue a sollicitudin. Cras non feugiat diam.',
        image: require('./../../assets/background/banner-bg.jpg'),
    },
    {
        title: 'Lorem Ipsum 3',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ex ullamcorper, mattis ipsum facilisis, eleifend purus. Suspendisse malesuada sed ex vel cursus. Proin augue massa, lacinia quis magna non, ultrices vehicula mi. Ut vulputate tellus in egestas fermentum. Maecenas arcu ante, imperdiet sed ullamcorper at, rhoncus quis neque. Phasellus leo odio, malesuada sed mi sit amet, blandit ullamcorper tellus. Integer quis tortor tincidunt, mattis mauris nec, molestie sapien. Pellentesque eu maximus mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris gravida accumsan augue a sollicitudin. Cras non feugiat diam.',
        image: require('./../../assets/background/banner-bg.jpg'),
    }
]