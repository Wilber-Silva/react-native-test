import React from "react";
import {StyleSheet, ScrollView, View} from "react-native";

import {SideBarMenu} from "./../components/Drawer";
import {
    BannerSession, 
    SpotlightSession, 
    SessionTitle
} from "./../components/Sessions";

const styles = StyleSheet.create({
    scrollView:{
      height: '100%',
      backgroundColor: '#F5FCFF',
    },
    childScrollView:{
      width: '100%',
      backgroundColor: '#F5FCFF',
    },
    childScrollViewContainerStyle: {
      alignItems: 'center',
      justifyContent: 'center',
    }
})

const Screen = () => (
  <SideBarMenu>
    <ScrollView style={styles.scrollView}>
        <BannerSession/>
        <View style={{marginLeft:20}}>
            <SessionTitle title='Destaque'/>
            <SpotlightSession
                style={styles.childrenStyle}
                contentStyle={styles.childrenContentStyle}
            />

            <SessionTitle title='Inspirações'/>
            <SpotlightSession
                style={styles.childrenStyle}
                contentStyle={styles.childrenContentStyle}
            />

            <SessionTitle title='Noticias da Duratex'/>
            <SpotlightSession
                style={styles.childrenStyle}
                contentStyle={styles.childrenContentStyle}
            />

            <SessionTitle title='Para a sua marcenaria'/>
            <SpotlightSession
                style={styles.childrenStyle}
                contentStyle={styles.childrenContentStyle}
            />
        </View>
    </ScrollView>
  </SideBarMenu>
)

export default Screen;