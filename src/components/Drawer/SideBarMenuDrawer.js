import React, { Component } from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import {Drawer, Container, Header} from 'native-base';

import SideBarContent from "./../SideBar/SideBarMenuContent";

const styles = StyleSheet.create({
  icon: {
    marginLeft: 15,
    marginTop: 15,
    marginBottom: 15,
    marginRight: 15,
    width: 30,
    height: 30,
  },
  text:{
    fontSize: 28,
    color: '#636466',
    marginTop: 5,
    marginBottom: 15,
    marginRight: 15,
  }
})

class SideBarMenu extends Component {
  closeDrawer = () => {
      this.drawer._root.close()
  };
  openDrawer = () => {
      this.drawer._root.open()
  };    
  render() {
    return (
        <Drawer
          ref={drawer => { this.drawer = drawer }}
          content={
            <SideBarContent 
              navigator={this.navigator} 
              close={this.closeDrawer}
            />
          }
          style={{width:'100%'}}
          onClose={() => this.closeDrawer()}>
        <Container>
          <Header>
              <Container style={{flexDirection: 'row'}}>
                <TouchableOpacity  onPress={() => this.openDrawer()}>
                  <Image 
                    style={styles.icon}
                    source={require('./../../assets/icons/bars.jpg')}
                  />
                </TouchableOpacity>
                <Text style={styles.text}>Clube d</Text>
              </Container>
          </Header>
            
            {this.props.children}
          
          </Container>
      </Drawer> 
    );
  }
}

export default SideBarMenu;