import React from 'react'
import {StyleSheet, View, Image, Text} from "react-native";

const styles = StyleSheet.create({
    container:{
        padding: 5,
        flex: 1,
    },
    image:{
        width: 222,
        height: 140,
        borderRadius: 10
    },
    title:{
        fontFamily: 'PF Square Sans Pro, Medium',
        fontSize: 18,
        color: '#4D4D4D',
        textAlign:'left',
        fontWeight: 'bold'
    },
    description:{
        fontFamily: 'PF Square Sans Pro, Medium',
        fontSize: 16,
        color: '#4D4D4D',
        textAlign:'left',
        height: 65,
        width: 222,
    },
})

const CardInfo = ({source, title, description}) => (
    <View style={styles.container}>
        <Image style={styles.image} source={source}/>
        <Text style={styles.title}> {title} </Text>
        <Text style={styles.description} ellipsizeMode='tail' numberOfLines={3}> 
            {description} 
        </Text>
    </View>
)

export default CardInfo;