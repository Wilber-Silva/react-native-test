import React from "react";
import {StyleSheet, View, ListView, Text, TouchableOpacity, Image} from "react-native";
import {ListItem as Avatar} from "react-native-elements";

import {sideBarList as data} from "./../../assets/data";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        width: '100%',
    },
    listContainer:{
        marginTop: 10,
        marginBottom: 10,
        height: 200,
    },
    rowContainer: {
        flex: 1,
        padding: 12,
        flexDirection: 'row',
    },
    rowText: {
        fontFamily: 'PF Square Sans Pro, Bold ',
        marginLeft: 5,
        padding: 5,
        fontSize: 16,
        fontWeight: 'bold'
    },
    avatar:{
        borderBottomColor:'#BCBCCB',
        borderBottomWidth: 1,
    },
    icon: {
        marginLeft: 15,
        marginTop: 15,
        marginBottom: 15,
        marginRight: 15,
        width: 30,
        height: 30,
    },
    text:{
        fontSize: 28,
        color: '#636466',
        marginTop: 5,
        marginBottom: 15,
        marginRight: 15,
      }
})

const DataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2  });

const listData = DataSource.cloneWithRows(data)

const Row = ({title, icon}) => (
    <View style={styles.rowContainer}>
        <Text style={styles.rowText}>{title}</Text>
    </View>
)

const SideBarContent = ({close}) => (
    <View style={styles.container}>

        <View style={{flexDirection: 'row'}}>
            <TouchableOpacity  onPress={() => close()}>
            <Image 
                style={styles.icon}
                source={require('./../../assets/icons/close.png')}
            />
            </TouchableOpacity>
            <Text style={styles.text}>Clube d</Text>
        </View>
        <Avatar
            style={styles.avatar}
            leftAvatar={{
                title: 'Loren ipsum',
                source:{
                    uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                },
            }}
            title={'Loren ipsum'}
        />
        <ListView
            style={styles.listContainer}
            dataSource={listData}
            renderRow={(data) => <Row {...data} />}
        />
    </View>
)

export default SideBarContent;