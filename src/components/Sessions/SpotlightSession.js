import React from "react";
import {ScrollView} from "react-native";

import {CardInfo as Card} from "./../Card";
import {fakeSpotlightData as items} from "./../../assets/data";

const SpotlightSession = ({style, contentStyle}) => (
    <ScrollView style={style} contentContainerStyle={contentStyle} horizontal>
        { items.map( ( item, key ) => (
            <Card 
                key={key}
                source={item.image}
                title={item.title}
                description={item.description}
            />
        ) ) }
    </ScrollView>
);

export default SpotlightSession;