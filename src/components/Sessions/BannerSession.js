import React from "react";
import { StyleSheet, ImageBackground, Text } from "react-native";

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 550,
        resizeMode: 'cover',
        flex: 1,
        alignSelf: 'stretch'
    },
    title:{
        fontFamily:'PF Square Sans Pro,Bold',
        fontSize: 57,
        color: '#FFF',
        marginTop: 69,
        marginLeft: 17,
        marginRight: 57
    },
    contentInformation:{
        fontFamily:'PF Square Sans Pro,Bold',
        fontSize:18,
        textAlign:'left',
        color: '#FFF',
        marginLeft: 17,
        marginRight: 18
    }
});

const Banner = () => (
    <ImageBackground 
        source={require('./../../assets/background/banner-bg.jpg')} 
        style={styles.image}
    >
        <Text style={styles.title}>Faça parte do clube</Text>
        <Text style={styles.contentInformation}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae turpis elementum, ultrices elit dignissim, gravida augue.
        </Text>
    </ImageBackground>
);

export default Banner;