import React from "react"
import {StyleSheet, Text} from "react-native"

const styles = StyleSheet.create({
    title:{
        fontFamily: 'PF Square Sans Pro, Medium',
        fontSize: 24,
        color: '#4D4D4D',
        textAlign:'left',
        fontWeight: 'bold',
    },
})

const Title = ({title, styleTitle}) => (
    <Text style={{...styles.title, ...styleTitle}}>{title}</Text>
)

Title.defaultProps = {
    styleTitle: {
        marginTop: 30,
        marginLeft: 5,
        marginBottom: 10,
    }
}

export default Title;